function BaseStorable(id) {
    this.id = id || Math.floor(new Date().getTime() * Math.random());
}

BaseStorable.loadAll = function() {
    let name = 'ktj' + this.name;
    if (localStorage[name]) {
        this.collection = JSON.parse(localStorage[name]).map(this.loadFromObject);
        return true;
    }
    this.collection = [];
    return false;
};

BaseStorable.loadFromObject = function (o) {
    throw new Error('Not implemented');
};

BaseStorable.prototype.save = function() {
    let name = 'ktj' + this.constructor.name;
    !this.constructor.collection.includes(this) && this.constructor.collection.push(this);
    localStorage[name] = JSON.stringify(this.constructor.collection);
};

// for objects that have a user
BaseStorable.prototype.getUser = function() {
    return User.collection.find(i => i.id === this.user);
};

function resetStorage() {
    localStorage.removeItem('ktjItem');
    localStorage.removeItem('ktjUser');
    localStorage.removeItem('ktjUserItem');
    localStorage.removeItem('ktjUserSrs');
}
