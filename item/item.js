function Item(word, reading, meaning, id) {
    BaseStorable.call(this, id);
    this.word = word;
    this.reading = reading;
    this.meaning = meaning;
}
Item.prototype = Object.create(BaseStorable.prototype);
Item.prototype.constructor = Item;
Item.loadAll = BaseStorable.loadAll;
Item.loadFromObject = function (o) {
    return new Item(o.word, o.reading, o.meaning, o.id);
}

Item.prototype.getReadings = function() {
    return this.reading.split(', ');
}

Item.prototype.getMeanings = function() {
    return this.meaning.split(', ');
}

/********************/

if (!Item.loadAll()) {
    let s = document.createElement('script');
    s.src = 'item/sample-items.js'
    s.onload = () => {
        Item.collection.push.apply(Item.collection, _items.map(Item.loadFromObject));
        Item.collection[0].save();
    }
    document.head.appendChild(s);
}
