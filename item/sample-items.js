const _items = [
    {"word": "人工", "reading": "じんこう", "meaning": "artificial, man made"},
    {"word": "入学", "reading": "にゅうがく", "meaning": "school admission"},
    {"word": "白血球", "reading": "はっけっきゅう", "meaning": "white blood cell"},
    {"word": "赤血球", "reading": "せっけっきゅう", "meaning": "red blood cell"},
    {"word": "血小板", "reading": "けっしょうばん", "meaning": "platelet"},
    {"word": "吸血鬼", "reading": "きゅうけつき", "meaning": "vampire, blood sucking demon"},
    {"word": "留守番電話", "reading": "るすばんでんわ", "meaning": "answering machine"},
    {"word": "保守主義", "reading": "ほしゅしゅぎ", "meaning": "conservativism"},
    {"word": "共産主義", "reading": "きょうさんしゅぎ", "meaning": "communism"},
    {"word": "罰ゲーム", "reading": "ばつげーむ", "meaning": "batsu game"}
];
