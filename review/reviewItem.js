function ReviewItem(userItemId, type, availableAt, answeredAt, answered, wrongly, id) {
    BaseStorable.call(this, id);
    this.userItem = userItemId;
    this.type = type; // 'reading' or 'meaning'
    this.availableAt = availableAt; // timestamp
    this.answeredAt = answeredAt || null;
    this.answered = answered || false;
    this.wrongly = wrongly || false;
}
ReviewItem.prototype = Object.create(BaseStorable.prototype);
ReviewItem.prototype.constructor = ReviewItem;
ReviewItem.loadAll = BaseStorable.loadAll;
ReviewItem.loadFromObject = function (o) {
    return new ReviewItem(o.userItem, o.type, o.availableAt, o.answeredAt, o.answered, o.wrongly, o.id);
};

ReviewItem.prototype.getUserItem = function () {
    return UserItem.collection.find(i => i.id === this.userItem);
};
