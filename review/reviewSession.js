function ReviewSession(userId, queue, answered, id) {
    BaseStorable.call(this, id);
    this.user = userId;
    this.queue = queue || [];
    this.answered = answered || [];
}
ReviewSession.prototype = Object.create(BaseStorable.prototype);
ReviewSession.prototype.constructor = ReviewSession;
ReviewSession.loadAll = BaseStorable.loadAll;
ReviewSession.loadFromObject = function(o) {
    return new ReviewSession(o.user, o.queue, o.answered, o.id);
};

ReviewSession.prototype.available = function() {
    return this.queue.length > 0;
};
