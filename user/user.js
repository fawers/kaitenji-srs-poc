function User(displayName, id) {
    BaseStorable.call(this, id);
    this.displayName = displayName;
}
User.prototype = Object.create(BaseStorable.prototype)
User.prototype.constructor = User;
User.loadAll = BaseStorable.loadAll;
User.loadFromObject = function(o) {
    return new User(o.displayName, o.id);
};

User.prototype.getItems = function() {
    return UserItem.collection.filter(i => i.user === this.id);
};

User.prototype.getSrsDefinition = function() {
    let userSrs = UserSrs.collection.find(i => i.user === this.id);
    if (userSrs === undefined) {
        userSrs = new UserSrs(this.id);
        userSrs.save();
    }
    return userSrs;
};
/********************/

if (!User.loadAll()) {
    new User('fawers').save(); new User('jinrohk').save();
}
