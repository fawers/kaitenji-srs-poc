/**
 * User 1:n UserItem
 * Item 1:n UserItem
 * ∴ User n:n Item
 */
function UserItem(userId, itemId, level, readingCorrect, readingWrong, meaningCorrect, meaningWrong, id) {
    BaseStorable.call(this, id);
    this.user = userId;
    this.item = itemId;
    this.level = level || 0;
    this.rCorrect = readingCorrect || 0;
    this.rWrong = readingWrong || 0;
    this.mCorrect = meaningCorrect || 0;
    this.mWrong = meaningWrong || 0;
}
UserItem.prototype = Object.create(BaseStorable.prototype);
UserItem.prototype.constructor = UserItem;
UserItem.loadAll = BaseStorable.loadAll;
UserItem.loadFromObject = function(o) {
    return new UserItem(o.user, o.item, o.level, o.rCorrect, o.rWrong, o.mCorrect, o.mWrong, o.id)
};

UserItem.prototype.getItem = function() {
    return Item.collection.find(i => i.id === this.item);
};

/********************/

UserItem.loadAll();
