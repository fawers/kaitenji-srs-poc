/**
 * User 1:1 UserSrs
 */
function UserSrs(userId, levelsArray, id) {
    BaseStorable.call(this, id);
    this.user = userId;
    this.levelsArray = levelsArray || [
        0, 3, 7, 23, 71, 167, 335, 719, 2159, 5039
    ];
}
UserSrs.prototype = Object.create(BaseStorable.prototype);
UserSrs.prototype.constructor = UserSrs;
UserSrs.loadAll = BaseStorable.loadAll;
UserSrs.loadFromObject = function(o) {
    return new UserSrs(o.user, o.levelsArray, o.id);
};

/********************/

UserSrs.loadAll();
